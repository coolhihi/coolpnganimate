/*
 * png实现动画类
 * 可控制帧数的自制动画
 * 帧图片横排
 * 可任意转载使用
 * 望保留以下版权
 *
 *    Developed by COoL  |  QQ:176512025  |  Email:176512025@qq.com
 *       _______     _______             _
 *      /  ______\  /  ___  `\    ___   / \
 *     /\  \       /\  \   \  \  / __`\/\  \
 *     \ \  \______\ \  \___\  \/\ \_\ \ \  \_______
 *      \ \________/\ \________/\ \____/\ \________/
 *       \/_______/  \/_______/  \/___/  \/_______/
 *
 */

var CoolPngAnimate = function (pngsrc, totalwidth, width, height, animateinterval, nostop, autohide) {
    this.pngsrc=pngsrc;//定义png图地址
    this.totalwidth=totalwidth;//png图总宽度
    this.width=width;//png图显示宽度
    this.height=height;//png图高度
    this.animateinterval=animateinterval;//定义动画开启后切换的间隔
    this.nostop=nostop;//自动循环
    this.autohide=autohide;//当执行倒序动画后自动隐藏
    
    this.pngdiv="";//保存该动态图片,即以该动态图为背景的div
    this.myinterval="";//保存循环便于clear
    this.nowwidth=0;//保存目前目前帧所在位置
    
    /*
     * 初始化对象
     */
    this.init = function(){
        this.pngdiv=document.createElement('div');
        $(this.pngdiv).css({'width':this.width+'px', 'height':this.height+'px', 'overflow':'hidden', 'display':'block','background-image':'url('+this.pngsrc+')','background-position':'0px, 0px'});
    };
    
    /*
     * 显示到指定图层中，以静止在默认帧的方式
     */
    this.show = function(containerid){
        $("#"+containerid).append(this.pngdiv);
    };
    
    /*
     * 从容器中删除，相对show
     */
    this.hide = function(){
        $(this.pngdiv).remove();
    };
    
    /*
     * 切换到指定帧,从1数起
     */
    this.to = function(n){
        this.nowwidth=this.width*(1-n);
        $(this.pngdiv).css('background-position',this.nowwidth+'px 0px');
    };
    
    /*
     * 开始动画
     */
    this.start = function(){
        clearInterval(this.myinterval);
        if(this.nowwidth==this.width-this.totalwidth) this.nowwidth=this.width;
        var _this=this;
        this.myinterval=setInterval(function(){
            _this.nowwidth-=_this.width;
            if(_this.nowwidth==_this.width-_this.totalwidth){
                //若到达最后帧(因为计算优先,所以坐标是超出一帧)
                if(!_this.nostop){
                    //若不开启循环,则停止并停留在最后帧
                    //_this.nowwidth=_this.width-_this.totalwidth;
                    clearInterval(_this.myinterval);
                }
                else{
                    //为避免会坐标出错,返回第一循环的坐标,继续循环
                    _this.nowwidth=0;
                }
            }
            $(_this.pngdiv).css('background-position',_this.nowwidth+'px 0px');
        },_this.animateinterval);
    };
    
    /*
     * 倒序开始动画
     */
    this.startb = function(){
        clearInterval(this.myinterval);
        if(this.nowwidth==0) this.nowwidth=0-this.totalwidth;
        var _this=this;
        this.myinterval=setInterval(function(){
            _this.nowwidth+=_this.width;
            if(_this.nowwidth==0){
                //若到达最后帧(因为计算优先,所以坐标是超出一帧)
                if(!_this.nostop){
                    //若不开启循环,则停止并停留在最后帧
                    //_this.nowwidth=0;
                    clearInterval(_this.myinterval);
                    
                    //若开启倒序自动隐藏,则在此时隐藏
                    if(_this.autohide)
                        _this.hide();
                }
                else{
                    //为避免会坐标出错,返回第一循环的坐标,继续循环
                    _this.nowwidth=0-_this.totalwidth;
                }
            }
            $(_this.pngdiv).css('background-position',_this.nowwidth+'px 0px');
        },_this.animateinterval);
    };
    
    /*
     * 停止动画并跳至预定义的结束帧
     */
    this.stop = function(){
        //this.nowwidth=this.width*(1-this.finalnum);
        clearInterval(this.myinterval);
        //$(this.pngdiv).css('background-position',this.nowwidth+'px 0px');
    };
};